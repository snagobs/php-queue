<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->char('original_photo');
            $table->char('photo_100_100');
            $table->char('photo_150_150');
            $table->char('photo_250_250');
            $table->enum('status',['UPLOADED', 'PROCESSING', 'SUCCESS', 'FAIL']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
