<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\PhotoService;
use Intervention\Image\Facades\Image;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $photoService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request, PhotoService $photoService)
    {
        $pathToPhoto = 'public/images/'. Auth()->id();
        $image = $request->file('image');

        $photo_100x100 = $photoService->crop($pathToPhoto, 100, 100);
        $photo_150x150 = $photoService->crop($pathToPhoto, 150, 150);
        $photo_250x250 = $photoService->crop($pathToPhoto, 250, 250);

        Image::make($photo_100x100)->save('public/images/'. Auth()->id()
            .'/'
            . $image->getFilename()
            .'100x100'
            .$image->getClientOriginalExtension()
        );

        Image::make($photo_150x150)->save('public/images/'. Auth()->id()
            .'/'
            . $image->getFilename()
            .'150x150'
            .$image->getClientOriginalExtension()
        );

        Image::make($photo_250x250)->save('public/images/'. Auth()->id()
            .'/'
            . $image->getFilename()
            .'250x250'
            .$image->getClientOriginalExtension()
        );
    }
}
