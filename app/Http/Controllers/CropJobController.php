<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\CropJob;

class CropJobController extends Controller
{
    public function processQueue()
    {
        $cropJob = new CropJob();
        dispatch($cropJob);
    }
}
