<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PhotoController extends Controller
{
    private $photo_ext = ['jpg', 'jpeg', 'png', 'gif'];

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $ext = implode(',', $this->photo_ext);

        $this->validate($request, [
            'name' => 'required|unique:files',
            'file' => 'required|file|mimes' . $ext
        ]);

        $fileName = request()->fileToUpload->getClientOriginalExtension();

        $request->fileToUpload->storeAs('public/images/'. Auth()->id(), $fileName);

        return back()
            ->with('success','You have successfully upload image.');
    }
}
