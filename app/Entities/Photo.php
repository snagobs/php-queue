<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = 'photos';
    protected $primaryKey = 'id';

    protected $fillable = ['id','user_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
